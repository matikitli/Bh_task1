package company.Interface;

import company.enums.RoleEnum;

public interface Report {
    String toString();
    String getOwnersName();
    String getOwnersSurname();
    RoleEnum getOwnersRole();
    int getOwnersUnitsOfWork();

}
