package company.Interface;

public interface Strategy {


    boolean canHireDependsOnStrategy(Employee employee);

    String getExplanation();
}
