package company.Interface;

import java.util.List;

/**
 * Created by matikitli on 21.07.17.
 */
public interface Manager extends Employee {
    List<Employee> hire(List<Employee> employee);
    void fire(List<Employee> employee);
    boolean canHire(Employee employee);

}
