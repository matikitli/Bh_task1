package company.Interface;

import company.enums.EmailAddress;
import company.enums.RoleEnum;
import company.report.ReportImpl;
import company.task.Task;

import java.util.List;

/**
 * Created by matikitli on 21.07.17.
 */
public interface Employee {

    void setName(String name);
    String getName();

    void setSurname(String surname);
    String getSurname();

    void setSex(String sex);
    String getSex();

    void setCountry(String country);
    String getCountry();

    void setUniversity(String university);
    String getUniversity();

    void setEmail(EmailAddress email);
    EmailAddress getEmail();

    void setRole(RoleEnum role);
    RoleEnum getRole();






    void assign(List<Task> task);
    List<ReportImpl> reportWork();
    String toString();
}
