package company.structure.manager;

import company.Interface.Employee;
import company.Interface.Manager;
import company.Interface.Strategy;
import company.generator.PersonGenerator;
import company.report.ReportComparator;
import company.report.ReportImpl;
import company.structure.employee.AbstractEmployee;
import company.task.Task;

import java.util.*;

/**
 * Created by matikitli on 21.07.17.
 */

public class TeamManager extends AbstractEmployee implements Manager {

    public static final String CLASSNAME = "MANAGER";

    private List<Employee> hiredEmployers = new ArrayList<>();
    private int numberOfHiredEmployers;
    private int numberOfEmployersThatCanBeHired;
    private List<Task> listOfTasks = new ArrayList<>(); //he can have tasks too
    private List<ReportImpl> listOfReportsFromEmployees = new ArrayList<>();
    private Strategy strategy;

    public TeamManager(PersonGenerator person, int numberOfEmployersThatCanBeHired){
        super(person);
        this.numberOfEmployersThatCanBeHired=numberOfEmployersThatCanBeHired;
    }

    @Override
    public List<Employee> hire(List<Employee> employee) {
        for(Employee e:employee) {
            if (canHire(e)) {
                hiredEmployers.add(e);
            } else {
                //System.out.println("Manager: "+ this.getName()+" can not hire: "+e.getName()+", only looking for persons "+strategy.getExplanation());
            }
        }
        return hiredEmployers;
    }

    @Override
    public void fire(List<Employee> employee) {
        for(Employee e:employee) {
            if (hiredEmployers.contains(e)) {
                hiredEmployers.remove(e);
            } else {
                System.out.println("There is no such employee hired");
            }
        }
    }

    @Override
    public boolean canHire(Employee employee) {
        if (this.hiredEmployers.size()==this.numberOfEmployersThatCanBeHired) {
            return false;
        }
       return strategy.canHireDependsOnStrategy(employee);
    }

    public void setStrategy(Strategy strategy){
        this.strategy=strategy;
    }

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append(super.toString());
        Iterator<Employee> iterator = hiredEmployers.iterator();
        while(iterator.hasNext()){
            Employee emp = iterator.next();
            builder.append(emp.toString());
        }
        return builder.toString();
    }

    public List<Employee> getHiredEmployers() {
        return hiredEmployers;
    }

    @Override
    public void assign(List<Task> task) {
        this.listOfTasks.addAll(task);
    }

    @Override
    public List<ReportImpl> reportWork(){
        List<ReportImpl> listOfReports = new ArrayList<>();
        ReportImpl report = new ReportImpl(this,listOfTasks);
        Iterator<Employee> employeeIterator = hiredEmployers.iterator();
        listOfReports.add(report);

        while(employeeIterator.hasNext()){
            Employee emp = employeeIterator.next();
            listOfReports.addAll(emp.reportWork());
        }

        Collections.sort(listOfReports,new ReportComparator());

        return listOfReports;
    }



}
