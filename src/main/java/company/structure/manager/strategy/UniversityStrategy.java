package company.structure.manager.strategy;

import company.Interface.Employee;
import company.Interface.Strategy;
import company.enums.UniversityEnum;

public class UniversityStrategy implements Strategy{
    @Override
    public boolean canHireDependsOnStrategy(Employee employee) {
        return employee.getUniversity().equalsIgnoreCase(UniversityEnum.AGH.toString());
    }

    @Override
    public String getExplanation() {
        return "who studied at "+UniversityEnum.AGH;
    }
}
