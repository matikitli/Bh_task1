package company.structure.manager.strategy;

import company.Interface.Employee;
import company.Interface.Strategy;

public class EmailStrategy implements Strategy {
    private final String CRITERIA="gmail.com";
    @Override
    public boolean canHireDependsOnStrategy(Employee employee) {
        return employee.getEmail().getSuffix().equalsIgnoreCase(CRITERIA);
    }

    @Override
    public String getExplanation() {
        return "with email suffix "+CRITERIA;
    }


}
