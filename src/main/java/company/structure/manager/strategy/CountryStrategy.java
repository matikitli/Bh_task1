package company.structure.manager.strategy;

import company.Interface.Employee;
import company.Interface.Strategy;
import company.enums.CountryEnum;

public class CountryStrategy implements Strategy {

    @Override
    public boolean canHireDependsOnStrategy(Employee employee) {
        return employee.getCountry().equalsIgnoreCase(CountryEnum.POLAND.toString());
    }

    @Override
    public String getExplanation() {
        return "from "+CountryEnum.POLAND;
    }
}
