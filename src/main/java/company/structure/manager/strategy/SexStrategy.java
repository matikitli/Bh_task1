package company.structure.manager.strategy;

import company.Interface.Employee;
import company.Interface.Strategy;
import company.enums.SexEnum;

public class SexStrategy implements Strategy {

    @Override
    public boolean canHireDependsOnStrategy(Employee employee) {
       return employee.getSex().equalsIgnoreCase(SexEnum.MAN.toString());
    }

    @Override
    public String getExplanation() {
        return "with sex: "+SexEnum.MAN;
    }
}
