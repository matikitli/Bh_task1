package company.structure.manager.strategy;

import company.Interface.Employee;
import company.Interface.Strategy;

public class DefaultStrategy implements Strategy {
    @Override
    public boolean canHireDependsOnStrategy(Employee employee) {
        return true;
    }

    @Override
    public String getExplanation() {
        return "";
    }
}
