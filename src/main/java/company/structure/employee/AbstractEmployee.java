package company.structure.employee;

import company.Interface.Employee;
import company.enums.EmailAddress;
import company.enums.RoleEnum;
import company.generator.PersonGenerator;
import company.report.ReportImpl;
import company.task.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by matikitli on 21.07.17.
 */

public abstract class AbstractEmployee implements Employee {

    private String name;
    private String surname;
    private String sex;
    private String country;
    private String university;
    private EmailAddress email;
    private RoleEnum role;
    private List<Task> listOfTasks = new ArrayList<>();

    public AbstractEmployee(PersonGenerator person){
        this.name=person.getName();
        this.surname=person.getSurname();
        this.sex=person.getSex();
        this.country=person.getCountry();
        this.university=person.getUniversity();
        this.email=person.getEmailAddress();
        this.role=person.getRole();
    }

    @Override
    public void setName(String name) { this.name = name; }

    @Override
    public String getName() { return this.name; }

    @Override
    public String getSurname() { return surname; }

    @Override
    public void setSurname(String surname) { this.surname = surname; }

    @Override
    public String getSex() { return sex; }

    @Override
    public void setSex(String sex) { this.sex = sex; }

    @Override
    public String getCountry() { return country; }

    @Override
    public void setCountry(String country) { this.country = country; }

    @Override
    public String getUniversity() { return university; }

    @Override
    public void setUniversity(String university) { this.university = university; }

    @Override
    public EmailAddress getEmail() { return email; }

    @Override
    public void setEmail(EmailAddress email) { this.email = email; }

    @Override
    public void setRole(RoleEnum role) { this.role = role; }

    @Override
    public RoleEnum getRole() { return this.role; }

    @Override
    public String toString() {
            return getClass().getSimpleName().toUpperCase() +"{" +
                    "name='" + name + '\'' +
                    ", surname='" + surname + '\'' +
                    ", sex='" + sex + '\'' +
                    ", country='" + country + '\'' +
                    ", university='" + university + '\'' +
                    ", email=" + email +
                    ", role=" + role +
                    "}\n";

    }

    public void setListOfTasks(List<Task> listOfTasks) {
        this.listOfTasks = listOfTasks;
    }

    @Override
    public void assign(List<Task> task) {
        this.listOfTasks.addAll(task);
    }


    public List<ReportImpl> reportWork() {
        List<ReportImpl> reportList = new ArrayList<>();
        reportList.add(new ReportImpl(this,this.listOfTasks));
        return reportList;
    }


}
