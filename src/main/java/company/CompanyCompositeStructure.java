package company;

import company.Interface.Employee;
import company.generator.PersonGenerator;
import company.generator.StructureGenerator;
import company.generator.TaskGenerator;
import company.structure.employee.Developer;
import company.structure.employee.Tester;
import company.structure.manager.strategy.CountryStrategy;
import company.structure.manager.strategy.DefaultStrategy;
import company.structure.manager.strategy.SexStrategy;
import company.structure.manager.strategy.UniversityStrategy;
import company.structure.manager.TeamManager;
import company.task.Task;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Created by matikitli on 22.07.17.
 */
public class CompanyCompositeStructure {

    public static void main(String[] args) {

        StructureGenerator structureOfCompany = new StructureGenerator(15);
        TaskGenerator taskGenerator = new TaskGenerator();
        TeamManager CEO = new TeamManager(new PersonGenerator(),structureOfCompany.getNumberOfTeamManagers());
        CEO.setStrategy(new DefaultStrategy());
        System.out.println(structureOfCompany.toString());
        List<Employee> listOfTeamManagers = structureOfCompany.generateManagers();
        CEO.hire(listOfTeamManagers);
        List<Employee> listOfPotentialEmployers = structureOfCompany.generatePotentialEmployees(30);
        for(Employee teamManager:listOfTeamManagers){
            List<Employee> hired = ((TeamManager) teamManager).hire(listOfPotentialEmployers);
            listOfPotentialEmployers.remove(hired);
        }
        for(TeamManager tm:(List<TeamManager>)(List<?>)listOfTeamManagers){
            tm.assign(taskGenerator.generateRandomTasks());
            for(Employee emp:tm.getHiredEmployers()){
                emp.assign(taskGenerator.generateRandomTasks());
            }
        }
        System.out.println();
        System.out.print(CEO.toString()+"\n");
        System.out.print("SORTED REPORTS\n"+CEO.reportWork());

    }
}
