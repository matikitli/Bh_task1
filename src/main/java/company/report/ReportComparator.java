package company.report;

import com.google.common.collect.ComparisonChain;

import java.util.Comparator;

public final class ReportComparator implements Comparator<ReportImpl> {

    public ReportComparator(){}

    @Override
    public int compare(ReportImpl o1, ReportImpl o2) {
    return ComparisonChain.start()
            .compare(o1.getOwnersSurname(),o2.getOwnersSurname())
            .compare(o1.getOwnersName(),o2.getOwnersName())
            .compare(o1.getOwnersRole().getRoleEnumPriority(),o2.getOwnersRole().getRoleEnumPriority())
            .compare(o1.getOwnersUnitsOfWork(),o2.getOwnersUnitsOfWork())
            .result();
    }
}
