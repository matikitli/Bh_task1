package company.report;

import company.Interface.Employee;
import company.Interface.Report;
import company.enums.RoleEnum;
import company.task.Task;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ReportImpl implements Report{

    private Employee employee;
    private List<Task> listOfTasks = new ArrayList<>();

    public ReportImpl(Employee employee, List<Task> listOfTasks){
        this.employee=employee;
        this.listOfTasks=listOfTasks;
    }

    @Override
    public String toString() {
        return "{" +
                "surname='" + employee.getSurname() + '\'' +
                ", name='" + employee.getName()+ '\'' +
                ", role='" + employee.getRole() + '\'' +
                ", unitsOfWork='" + getOwnersUnitsOfWork() + '\'' +
                ", listOfTasks=" + listOfTasks +
                '}'+'\n';
    }

    @Override
    public String getOwnersName() {
        return employee.getName();
    }

    @Override
    public String getOwnersSurname() {
        return employee.getSurname();
    }

    @Override
    public RoleEnum getOwnersRole() {
        return employee.getRole();
    }

    @Override
    public int getOwnersUnitsOfWork() {
        int unitsOfWork=0;
        Iterator<Task> iterator =listOfTasks.iterator();
        while (iterator.hasNext()){
            Task task = iterator.next();
            unitsOfWork += task.getHoursOfWork();
        }
        return unitsOfWork;
    }

}
