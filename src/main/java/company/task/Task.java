package company.task;

import java.util.Optional;

/**
 * Created by matikitli on 21.07.17.
 */
public class Task {
    private int hoursOfWork;
    private String job;

    public Task(int hours, String jobToDo){
        this.hoursOfWork = hours;
        this.job=jobToDo;

    }

    public int getHoursOfWork(){
        return this.hoursOfWork;
    }
    public String getJob(){ return job;}

    public String toString(){
        return String.format("%s, that takes: %d hours",getJob(),hoursOfWork);
    }



}
