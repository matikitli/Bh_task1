package company.generator;

import company.Interface.Employee;
import company.enums.EmailAddress;
import company.enums.RoleEnum;

import javax.management.relation.Role;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class PersonGenerator {
    private static List<String> firstnameManList = Arrays.asList("Szymek","Bartek","Kamil","Dawid","Grzesiek","Lukasz","Mateusz");
    private static List<String> firstnameWomanList = Arrays.asList("Natalia","Sandra","Kasia","Ola","Ania");
    private static List<String> surnameList = Arrays.asList("Pasikonik","Rybka","Bak","Trzmiel","Bulwa","Myszka","Orzech","Piasek");
    private static List<String> sexList = Arrays.asList("man","woman");
    private static List<String> countryList = Arrays.asList("Poland","Deutschland","England","USA");
    private static List<String> universityList = Arrays.asList("AGH","UJ","PK","PW");
    private static List<String> emailPrefixList = Arrays.asList("cebulka12","dupa666","kociak69","xTerminatorX","1234lol1234","petarda77","pompa66","koksik99");
    private static List<String> emailSufixList = Arrays.asList("gmail.com","onet.pl","interia.pl","wp.pl");

    private Random random = new Random();

    private String name;
    private String surname;
    private String sex;
    private String country;
    private String university;
    private EmailAddress emailAddress;
    private RoleEnum role;


    public PersonGenerator(){
        this.sex = sexList.get(randomFromRange(sexList.size()));
        if(sex=="man"){
            this.name=firstnameManList.get(randomFromRange(firstnameManList.size()));
        }
        else{
            this.name=firstnameWomanList.get(randomFromRange(firstnameWomanList.size()));
        }
        this.surname=surnameList.get(randomFromRange(surnameList.size()));
        this.country=countryList.get(randomFromRange(countryList.size()));
        this.university=universityList.get(randomFromRange(universityList.size()));
        this.emailAddress=new EmailAddress(emailPrefixList.get(randomFromRange(emailPrefixList.size())),emailSufixList.get(randomFromRange(emailSufixList.size())));
        this.role=RoleEnum.randomRole();
    }

    private int randomFromRange(int range){
        return random.nextInt(range);
    }


    public String getName() {
        return name;
    }

    public String getSurname() { return surname; }

    public String getSex() {
        return sex;
    }

    public String getCountry() {
        return country;
    }

    public String getUniversity() {
        return university;
    }

    public EmailAddress getEmailAddress() {
        return emailAddress;
    }

    public RoleEnum getRole() {
        return role;
    }

}
