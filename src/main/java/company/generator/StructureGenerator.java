package company.generator;

import com.sun.prism.shader.Solid_TextureRGB_AlphaTest_Loader;
import company.Interface.Employee;
import company.Interface.Strategy;
import company.structure.employee.Developer;
import company.structure.employee.Tester;
import company.structure.manager.TeamManager;
import company.structure.manager.strategy.*;
import sun.plugin2.gluegen.runtime.StructAccessor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class StructureGenerator {
    private int sizeOfCompany;
    private int numberOfTeamManagers;
    private int numberOfEmployers;
    private int numberOfTesters;
    private int numberOfDevelopers;

    private List<Integer> numberOfEmployeesThatManagerCanHire = new ArrayList<>();
    private final List<Strategy> listOfStrategies = Arrays.asList(new UniversityStrategy(),new CountryStrategy(),new SexStrategy(),new EmailStrategy());

    private List<TeamManager> listOfTeamManagers = new ArrayList<>();
    private List<Employee> listOfEmployers; //include testers+developers
    private List<Tester> listOfTesters;
    private List<Developer> listOfDelevopers;
    private Random random = new Random();


    public StructureGenerator(int sizeOfCompany){
        this.sizeOfCompany=sizeOfCompany+1;
        this.numberOfTeamManagers=sizeOfCompany/4;
        this.numberOfEmployers=sizeOfCompany-numberOfTeamManagers;
        this.numberOfTesters=numberOfEmployers/2;
        this.numberOfDevelopers=numberOfEmployers-numberOfTesters;
        setSizeOfTeamsForManagers();
    }


    @Override
    public String toString() {
        return "Structure:" +
                "\nSizeOfCompany=" + (sizeOfCompany-1) + " +  CEO"+
                "\nNumberOfTeamManagers=" + numberOfTeamManagers +
                ", that can hire: "+numberOfEmployeesThatManagerCanHire+
                "\nNumberOfEmployees=" + numberOfEmployers +
                ", including: Testers=" + numberOfTesters +
                " & Developers=" + numberOfDevelopers+"\n";
    }

    public int getNumberOfTeamManagers(){return numberOfTeamManagers;}

    public List<Employee> generatePotentialEmployees(int howMany) {
        List<Employee> potentialEmployeeList = new ArrayList<>();
        for (int i = 0; i < howMany; i++) {
            if(i%2==0){
                potentialEmployeeList.add(new Developer(new PersonGenerator()));
            }
            else potentialEmployeeList.add(new Tester(new PersonGenerator()));

        }
        return potentialEmployeeList;
    }

    public List<Employee> generateManagers() {
        List<Employee> list = new ArrayList<>();
        for (int i = 0; i < numberOfTeamManagers; i++) {
            TeamManager teamManager = new TeamManager(new PersonGenerator(),numberOfEmployeesThatManagerCanHire.get(i));
            teamManager.setStrategy(listOfStrategies.get(randomFromRange(listOfStrategies.size())));
            list.add(teamManager);
        }
        return list;
    }



    private void setSizeOfTeamsForManagers(){
        int averageSizeOfTeam = numberOfEmployers/numberOfTeamManagers;
        for(int i=0;i<numberOfTeamManagers;i++){
            numberOfEmployeesThatManagerCanHire.add(averageSizeOfTeam);
        }
        int left=numberOfEmployers-(numberOfTeamManagers*averageSizeOfTeam);
        int newSize = numberOfEmployeesThatManagerCanHire.get(1)+left;
        numberOfEmployeesThatManagerCanHire.remove(1);
        numberOfEmployeesThatManagerCanHire.add(newSize);
    }

    private int randomFromRange(int range){
        return random.nextInt(range);
    }
}
