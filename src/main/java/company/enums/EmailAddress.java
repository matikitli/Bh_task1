package company.enums;

public class EmailAddress {

    private String prefix;
    private String suffix;

    public EmailAddress(){}

    public EmailAddress(String prefix, String suffix){
        this.prefix=prefix;
        this.suffix=suffix;
    }

    @Override
    public String toString(){
        StringBuilder stringBuilder = new StringBuilder(prefix);
        stringBuilder.append("@");
        stringBuilder.append(suffix);
        return stringBuilder.toString();
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }
}
