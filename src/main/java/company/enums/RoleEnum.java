package company.enums;

import java.util.*;

/**
 * Created by matikitli on 21.07.17.
 */
public enum RoleEnum {
    //1-max 10-min
    DEVELOPER(5),
    TEAM_LEADER(3),
    DIRECTOR(1),
    MANAGER(2),
    TESTER(6),
    CONTRIBUTOR(7),
    SENIOR_DEVELOPER(4);

    private final int priority;
    private RoleEnum(final int priority){this.priority=priority;}
    public int getRoleEnumPriority(){return this.priority;}

    private static final List<RoleEnum> ROLES= Collections.unmodifiableList(Arrays.asList(values()));
    private static final int SIZE = ROLES.size();
    private static final Random RAND = new Random();
    public static RoleEnum randomRole(){
        return ROLES.get(RAND.nextInt(SIZE));
    }
}


